// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.model;

public class LeaveInfoWithBalance
{
    private CoStaffEntity staffInfo;
    private double bal;
    private double balSickLeave;
    private double eligibleMonth;
    private LeaveInfoEntity leaveInfo;
    
    public CoStaffEntity getStaffInfo() {
        return this.staffInfo;
    }
    
    public double getBal() {
        return this.bal;
    }
    
    public double getBalSickLeave() {
        return this.balSickLeave;
    }
    
    public double getEligibleMonth() {
        return this.eligibleMonth;
    }
    
    public LeaveInfoEntity getLeaveInfo() {
        return this.leaveInfo;
    }
    
    public void setStaffInfo(final CoStaffEntity staffInfo) {
        this.staffInfo = staffInfo;
    }
    
    public void setBal(final double bal) {
        this.bal = bal;
    }
    
    public void setBalSickLeave(final double balSickLeave) {
        this.balSickLeave = balSickLeave;
    }
    
    public void setEligibleMonth(final double eligibleMonth) {
        this.eligibleMonth = eligibleMonth;
    }
    
    public void setLeaveInfo(final LeaveInfoEntity leaveInfo) {
        this.leaveInfo = leaveInfo;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof LeaveInfoWithBalance)) {
            return false;
        }
        final LeaveInfoWithBalance other = (LeaveInfoWithBalance)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$staffInfo = this.getStaffInfo();
        final Object other$staffInfo = other.getStaffInfo();
        Label_0065: {
            if (this$staffInfo == null) {
                if (other$staffInfo == null) {
                    break Label_0065;
                }
            }
            else if (this$staffInfo.equals(other$staffInfo)) {
                break Label_0065;
            }
            return false;
        }
        if (Double.compare(this.getBal(), other.getBal()) != 0) {
            return false;
        }
        if (Double.compare(this.getBalSickLeave(), other.getBalSickLeave()) != 0) {
            return false;
        }
        if (Double.compare(this.getEligibleMonth(), other.getEligibleMonth()) != 0) {
            return false;
        }
        final Object this$leaveInfo = this.getLeaveInfo();
        final Object other$leaveInfo = other.getLeaveInfo();
        if (this$leaveInfo == null) {
            if (other$leaveInfo == null) {
                return true;
            }
        }
        else if (this$leaveInfo.equals(other$leaveInfo)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof LeaveInfoWithBalance;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $staffInfo = this.getStaffInfo();
        result = result * 59 + (($staffInfo == null) ? 43 : $staffInfo.hashCode());
        final long $bal = Double.doubleToLongBits(this.getBal());
        result = result * 59 + (int)($bal >>> 32 ^ $bal);
        final long $balSickLeave = Double.doubleToLongBits(this.getBalSickLeave());
        result = result * 59 + (int)($balSickLeave >>> 32 ^ $balSickLeave);
        final long $eligibleMonth = Double.doubleToLongBits(this.getEligibleMonth());
        result = result * 59 + (int)($eligibleMonth >>> 32 ^ $eligibleMonth);
        final Object $leaveInfo = this.getLeaveInfo();
        result = result * 59 + (($leaveInfo == null) ? 43 : $leaveInfo.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "LeaveInfoWithBalance(staffInfo=" + this.getStaffInfo() + ", bal=" + this.getBal() + ", balSickLeave=" + this.getBalSickLeave() + ", eligibleMonth=" + this.getEligibleMonth() + ", leaveInfo=" + this.getLeaveInfo() + ")";
    }
}
