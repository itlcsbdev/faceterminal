// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.model;

public class EmailMessage
{
    private String to_address;
    private String subject;
    private String body;
    private String senderEmail;
    private String senderPassword;
    
    public String getTo_address() {
        return this.to_address;
    }
    
    public void setTo_address(final String to_address) {
        this.to_address = to_address;
    }
    
    public String getSubject() {
        return this.subject;
    }
    
    public void setSubject(final String subject) {
        this.subject = subject;
    }
    
    public String getBody() {
        return this.body;
    }
    
    public void setBody(final String body) {
        this.body = body;
    }
    
    public String getSenderEmail() {
        return this.senderEmail;
    }
    
    public void setSenderEmail(final String senderEmail) {
        this.senderEmail = senderEmail;
    }
    
    public String getSenderPassword() {
        return this.senderPassword;
    }
    
    public void setSenderPassword(final String senderPassword) {
        this.senderPassword = senderPassword;
    }
}
