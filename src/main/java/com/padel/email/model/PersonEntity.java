// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.model;

import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Id;
import java.sql.Timestamp;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name = "person", schema = "eleave", catalog = "")
public class PersonEntity
{
    private int id;
    private int personGroupId;
    private int personType;
    private String personCode;
    private String givenName;
    private String familyName;
    private String fullName;
    private int gender;
    private String password;
    private String email;
    private String phoneNumber;
    private String photoUrl;
    private int photoIndex;
    private String smallPhotoUrl;
    private Timestamp startValidDate;
    private Timestamp endValidDate;
    private int type;
    private String remark;
    private Timestamp updateTime;
    private Timestamp createTime;
    private int isDeleted;
    private String objectGuid;
    private String temper;
    private int temperStatus;
    private int personFrom;
    private String rdn;
    private String dn;
    private long usnChanged;
    private int startTimeDiffer;
    private int endTimeDiffer;
    
    @Id
    @Column(name = "id")
    public int getId() {
        return this.id;
    }
    
    public void setId(final int id) {
        this.id = id;
    }
    
    @Basic
    @Column(name = "person_group_id")
    public int getPersonGroupId() {
        return this.personGroupId;
    }
    
    public void setPersonGroupId(final int personGroupId) {
        this.personGroupId = personGroupId;
    }
    
    @Basic
    @Column(name = "person_type")
    public int getPersonType() {
        return this.personType;
    }
    
    public void setPersonType(final int personType) {
        this.personType = personType;
    }
    
    @Basic
    @Column(name = "person_code")
    public String getPersonCode() {
        return this.personCode;
    }
    
    public void setPersonCode(final String personCode) {
        this.personCode = personCode;
    }
    
    @Basic
    @Column(name = "given_name")
    public String getGivenName() {
        return this.givenName;
    }
    
    public void setGivenName(final String givenName) {
        this.givenName = givenName;
    }
    
    @Basic
    @Column(name = "family_name")
    public String getFamilyName() {
        return this.familyName;
    }
    
    public void setFamilyName(final String familyName) {
        this.familyName = familyName;
    }
    
    @Basic
    @Column(name = "full_name")
    public String getFullName() {
        return this.fullName;
    }
    
    public void setFullName(final String fullName) {
        this.fullName = fullName;
    }
    
    @Basic
    @Column(name = "gender")
    public int getGender() {
        return this.gender;
    }
    
    public void setGender(final int gender) {
        this.gender = gender;
    }
    
    @Basic
    @Column(name = "password")
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(final String password) {
        this.password = password;
    }
    
    @Basic
    @Column(name = "email")
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(final String email) {
        this.email = email;
    }
    
    @Basic
    @Column(name = "phone_number")
    public String getPhoneNumber() {
        return this.phoneNumber;
    }
    
    public void setPhoneNumber(final String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    @Basic
    @Column(name = "photo_url")
    public String getPhotoUrl() {
        return this.photoUrl;
    }
    
    public void setPhotoUrl(final String photoUrl) {
        this.photoUrl = photoUrl;
    }
    
    @Basic
    @Column(name = "photo_index")
    public int getPhotoIndex() {
        return this.photoIndex;
    }
    
    public void setPhotoIndex(final int photoIndex) {
        this.photoIndex = photoIndex;
    }
    
    @Basic
    @Column(name = "small_photo_url")
    public String getSmallPhotoUrl() {
        return this.smallPhotoUrl;
    }
    
    public void setSmallPhotoUrl(final String smallPhotoUrl) {
        this.smallPhotoUrl = smallPhotoUrl;
    }
    
    @Basic
    @Column(name = "start_valid_date")
    public Timestamp getStartValidDate() {
        return this.startValidDate;
    }
    
    public void setStartValidDate(final Timestamp startValidDate) {
        this.startValidDate = startValidDate;
    }
    
    @Basic
    @Column(name = "end_valid_date")
    public Timestamp getEndValidDate() {
        return this.endValidDate;
    }
    
    public void setEndValidDate(final Timestamp endValidDate) {
        this.endValidDate = endValidDate;
    }
    
    @Basic
    @Column(name = "type")
    public int getType() {
        return this.type;
    }
    
    public void setType(final int type) {
        this.type = type;
    }
    
    @Basic
    @Column(name = "remark")
    public String getRemark() {
        return this.remark;
    }
    
    public void setRemark(final String remark) {
        this.remark = remark;
    }
    
    @Basic
    @Column(name = "update_time")
    public Timestamp getUpdateTime() {
        return this.updateTime;
    }
    
    public void setUpdateTime(final Timestamp updateTime) {
        this.updateTime = updateTime;
    }
    
    @Basic
    @Column(name = "create_time")
    public Timestamp getCreateTime() {
        return this.createTime;
    }
    
    public void setCreateTime(final Timestamp createTime) {
        this.createTime = createTime;
    }
    
    @Basic
    @Column(name = "is_deleted")
    public int getIsDeleted() {
        return this.isDeleted;
    }
    
    public void setIsDeleted(final int isDeleted) {
        this.isDeleted = isDeleted;
    }
    
    @Basic
    @Column(name = "object_guid")
    public String getObjectGuid() {
        return this.objectGuid;
    }
    
    public void setObjectGuid(final String objectGuid) {
        this.objectGuid = objectGuid;
    }
    
    @Basic
    @Column(name = "temper")
    public String getTemper() {
        return this.temper;
    }
    
    public void setTemper(final String temper) {
        this.temper = temper;
    }
    
    @Basic
    @Column(name = "temper_status")
    public int getTemperStatus() {
        return this.temperStatus;
    }
    
    public void setTemperStatus(final int temperStatus) {
        this.temperStatus = temperStatus;
    }
    
    @Basic
    @Column(name = "person_from")
    public int getPersonFrom() {
        return this.personFrom;
    }
    
    public void setPersonFrom(final int personFrom) {
        this.personFrom = personFrom;
    }
    
    @Basic
    @Column(name = "rdn")
    public String getRdn() {
        return this.rdn;
    }
    
    public void setRdn(final String rdn) {
        this.rdn = rdn;
    }
    
    @Basic
    @Column(name = "dn")
    public String getDn() {
        return this.dn;
    }
    
    public void setDn(final String dn) {
        this.dn = dn;
    }
    
    @Basic
    @Column(name = "usn_changed")
    public long getUsnChanged() {
        return this.usnChanged;
    }
    
    public void setUsnChanged(final long usnChanged) {
        this.usnChanged = usnChanged;
    }
    
    @Basic
    @Column(name = "start_time_differ")
    public int getStartTimeDiffer() {
        return this.startTimeDiffer;
    }
    
    public void setStartTimeDiffer(final int startTimeDiffer) {
        this.startTimeDiffer = startTimeDiffer;
    }
    
    @Basic
    @Column(name = "end_time_differ")
    public int getEndTimeDiffer() {
        return this.endTimeDiffer;
    }
    
    public void setEndTimeDiffer(final int endTimeDiffer) {
        this.endTimeDiffer = endTimeDiffer;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final PersonEntity that = (PersonEntity)o;
        return this.id == that.id && this.personGroupId == that.personGroupId && this.personType == that.personType && this.gender == that.gender && this.photoIndex == that.photoIndex && this.type == that.type && this.isDeleted == that.isDeleted && this.temperStatus == that.temperStatus && this.personFrom == that.personFrom && this.usnChanged == that.usnChanged && this.startTimeDiffer == that.startTimeDiffer && this.endTimeDiffer == that.endTimeDiffer && Objects.equals(this.personCode, that.personCode) && Objects.equals(this.givenName, that.givenName) && Objects.equals(this.familyName, that.familyName) && Objects.equals(this.fullName, that.fullName) && Objects.equals(this.password, that.password) && Objects.equals(this.email, that.email) && Objects.equals(this.phoneNumber, that.phoneNumber) && Objects.equals(this.photoUrl, that.photoUrl) && Objects.equals(this.smallPhotoUrl, that.smallPhotoUrl) && Objects.equals(this.startValidDate, that.startValidDate) && Objects.equals(this.endValidDate, that.endValidDate) && Objects.equals(this.remark, that.remark) && Objects.equals(this.updateTime, that.updateTime) && Objects.equals(this.createTime, that.createTime) && Objects.equals(this.objectGuid, that.objectGuid) && Objects.equals(this.temper, that.temper) && Objects.equals(this.rdn, that.rdn) && Objects.equals(this.dn, that.dn);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.personGroupId, this.personType, this.personCode, this.givenName, this.familyName, this.fullName, this.gender, this.password, this.email, this.phoneNumber, this.photoUrl, this.photoIndex, this.smallPhotoUrl, this.startValidDate, this.endValidDate, this.type, this.remark, this.updateTime, this.createTime, this.isDeleted, this.objectGuid, this.temper, this.temperStatus, this.personFrom, this.rdn, this.dn, this.usnChanged, this.startTimeDiffer, this.endTimeDiffer);
    }
}
