// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.model;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name = "leave_status", schema = "eleave", catalog = "")
public class LeaveStatusEntity
{
    private int id;
    
    @Id
    @Column(name = "id")
    public int getId() {
        return this.id;
    }
    
    public void setId(final int id) {
        this.id = id;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final LeaveStatusEntity that = (LeaveStatusEntity)o;
        return this.id == that.id;
    }
    
    @Override
    public int hashCode() {
        return this.id;
    }
}
