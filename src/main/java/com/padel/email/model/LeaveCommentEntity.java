// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name = "leave_comment", schema = "eleave", catalog = "")
public class LeaveCommentEntity
{
    private int id;
    private String leaveId;
    private String comment;
    private String date;
    private String time;
    private String staffId;
    
    @Id
    @Column(name = "id")
    public int getId() {
        return this.id;
    }
    
    public void setId(final int id) {
        this.id = id;
    }
    
    @Basic
    @Column(name = "leaveID")
    public String getLeaveId() {
        return this.leaveId;
    }
    
    public void setLeaveId(final String leaveId) {
        this.leaveId = leaveId;
    }
    
    @Basic
    @Column(name = "comment")
    public String getComment() {
        return this.comment;
    }
    
    public void setComment(final String comment) {
        this.comment = comment;
    }
    
    @Basic
    @Column(name = "date")
    public String getDate() {
        return this.date;
    }
    
    public void setDate(final String date) {
        this.date = date;
    }
    
    @Basic
    @Column(name = "time")
    public String getTime() {
        return this.time;
    }
    
    public void setTime(final String time) {
        this.time = time;
    }
    
    @Basic
    @Column(name = "staffID")
    public String getStaffId() {
        return this.staffId;
    }
    
    public void setStaffId(final String staffId) {
        this.staffId = staffId;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final LeaveCommentEntity that = (LeaveCommentEntity)o;
        if (this.id != that.id) {
            return false;
        }
        Label_0075: {
            if (this.leaveId != null) {
                if (this.leaveId.equals(that.leaveId)) {
                    break Label_0075;
                }
            }
            else if (that.leaveId == null) {
                break Label_0075;
            }
            return false;
        }
        Label_0108: {
            if (this.comment != null) {
                if (this.comment.equals(that.comment)) {
                    break Label_0108;
                }
            }
            else if (that.comment == null) {
                break Label_0108;
            }
            return false;
        }
        Label_0141: {
            if (this.date != null) {
                if (this.date.equals(that.date)) {
                    break Label_0141;
                }
            }
            else if (that.date == null) {
                break Label_0141;
            }
            return false;
        }
        Label_0174: {
            if (this.time != null) {
                if (this.time.equals(that.time)) {
                    break Label_0174;
                }
            }
            else if (that.time == null) {
                break Label_0174;
            }
            return false;
        }
        if (this.staffId != null) {
            if (this.staffId.equals(that.staffId)) {
                return true;
            }
        }
        else if (that.staffId == null) {
            return true;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        int result = this.id;
        result = 31 * result + ((this.leaveId != null) ? this.leaveId.hashCode() : 0);
        result = 31 * result + ((this.comment != null) ? this.comment.hashCode() : 0);
        result = 31 * result + ((this.date != null) ? this.date.hashCode() : 0);
        result = 31 * result + ((this.time != null) ? this.time.hashCode() : 0);
        result = 31 * result + ((this.staffId != null) ? this.staffId.hashCode() : 0);
        return result;
    }
}
