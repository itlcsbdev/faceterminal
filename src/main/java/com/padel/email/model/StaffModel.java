// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.model;

public class StaffModel
{
    private int userLevel;
    private CoStaffEntity coStaff;
    
    public int getUserLevel() {
        return this.userLevel;
    }
    
    public CoStaffEntity getCoStaff() {
        return this.coStaff;
    }
    
    public void setUserLevel(final int userLevel) {
        this.userLevel = userLevel;
    }
    
    public void setCoStaff(final CoStaffEntity coStaff) {
        this.coStaff = coStaff;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof StaffModel)) {
            return false;
        }
        final StaffModel other = (StaffModel)o;
        if (!other.canEqual((Object)this)) {
            return false;
        }
        if (this.getUserLevel() != other.getUserLevel()) {
            return false;
        }
        final Object this$coStaff = this.getCoStaff();
        final Object other$coStaff = other.getCoStaff();
        if (this$coStaff == null) {
            if (other$coStaff == null) {
                return true;
            }
        }
        else if (this$coStaff.equals(other$coStaff)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof StaffModel;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        result = result * 59 + this.getUserLevel();
        final Object $coStaff = this.getCoStaff();
        result = result * 59 + (($coStaff == null) ? 43 : $coStaff.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "StaffModel(userLevel=" + this.getUserLevel() + ", coStaff=" + this.getCoStaff() + ")";
    }
}
