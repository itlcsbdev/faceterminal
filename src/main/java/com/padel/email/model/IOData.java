// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.model;

public class IOData
{
    private String date;
    private String timein;
    private String timout;
    private String duration;
    private String colorduration;
    private String colortimein;
    private String colortimout;
    private boolean completeHour;
    private boolean lateIn;
    private boolean earlyOut;
    
    public String getDate() {
        return this.date;
    }
    
    public String getTimein() {
        return this.timein;
    }
    
    public String getTimout() {
        return this.timout;
    }
    
    public String getDuration() {
        return this.duration;
    }
    
    public String getColorduration() {
        return this.colorduration;
    }
    
    public String getColortimein() {
        return this.colortimein;
    }
    
    public String getColortimout() {
        return this.colortimout;
    }
    
    public boolean isCompleteHour() {
        return this.completeHour;
    }
    
    public boolean isLateIn() {
        return this.lateIn;
    }
    
    public boolean isEarlyOut() {
        return this.earlyOut;
    }
    
    public void setDate(final String date) {
        this.date = date;
    }
    
    public void setTimein(final String timein) {
        this.timein = timein;
    }
    
    public void setTimout(final String timout) {
        this.timout = timout;
    }
    
    public void setDuration(final String duration) {
        this.duration = duration;
    }
    
    public void setColorduration(final String colorduration) {
        this.colorduration = colorduration;
    }
    
    public void setColortimein(final String colortimein) {
        this.colortimein = colortimein;
    }
    
    public void setColortimout(final String colortimout) {
        this.colortimout = colortimout;
    }
    
    public void setCompleteHour(final boolean completeHour) {
        this.completeHour = completeHour;
    }
    
    public void setLateIn(final boolean lateIn) {
        this.lateIn = lateIn;
    }
    
    public void setEarlyOut(final boolean earlyOut) {
        this.earlyOut = earlyOut;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof IOData)) {
            return false;
        }
        final IOData other = (IOData)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$date = this.getDate();
        final Object other$date = other.getDate();
        Label_0065: {
            if (this$date == null) {
                if (other$date == null) {
                    break Label_0065;
                }
            }
            else if (this$date.equals(other$date)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$timein = this.getTimein();
        final Object other$timein = other.getTimein();
        Label_0102: {
            if (this$timein == null) {
                if (other$timein == null) {
                    break Label_0102;
                }
            }
            else if (this$timein.equals(other$timein)) {
                break Label_0102;
            }
            return false;
        }
        final Object this$timout = this.getTimout();
        final Object other$timout = other.getTimout();
        Label_0139: {
            if (this$timout == null) {
                if (other$timout == null) {
                    break Label_0139;
                }
            }
            else if (this$timout.equals(other$timout)) {
                break Label_0139;
            }
            return false;
        }
        final Object this$duration = this.getDuration();
        final Object other$duration = other.getDuration();
        Label_0176: {
            if (this$duration == null) {
                if (other$duration == null) {
                    break Label_0176;
                }
            }
            else if (this$duration.equals(other$duration)) {
                break Label_0176;
            }
            return false;
        }
        final Object this$colorduration = this.getColorduration();
        final Object other$colorduration = other.getColorduration();
        Label_0213: {
            if (this$colorduration == null) {
                if (other$colorduration == null) {
                    break Label_0213;
                }
            }
            else if (this$colorduration.equals(other$colorduration)) {
                break Label_0213;
            }
            return false;
        }
        final Object this$colortimein = this.getColortimein();
        final Object other$colortimein = other.getColortimein();
        Label_0250: {
            if (this$colortimein == null) {
                if (other$colortimein == null) {
                    break Label_0250;
                }
            }
            else if (this$colortimein.equals(other$colortimein)) {
                break Label_0250;
            }
            return false;
        }
        final Object this$colortimout = this.getColortimout();
        final Object other$colortimout = other.getColortimout();
        if (this$colortimout == null) {
            if (other$colortimout == null) {
                return this.isCompleteHour() == other.isCompleteHour() && this.isLateIn() == other.isLateIn() && this.isEarlyOut() == other.isEarlyOut();
            }
        }
        else if (this$colortimout.equals(other$colortimout)) {
            return this.isCompleteHour() == other.isCompleteHour() && this.isLateIn() == other.isLateIn() && this.isEarlyOut() == other.isEarlyOut();
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof IOData;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $date = this.getDate();
        result = result * 59 + (($date == null) ? 43 : $date.hashCode());
        final Object $timein = this.getTimein();
        result = result * 59 + (($timein == null) ? 43 : $timein.hashCode());
        final Object $timout = this.getTimout();
        result = result * 59 + (($timout == null) ? 43 : $timout.hashCode());
        final Object $duration = this.getDuration();
        result = result * 59 + (($duration == null) ? 43 : $duration.hashCode());
        final Object $colorduration = this.getColorduration();
        result = result * 59 + (($colorduration == null) ? 43 : $colorduration.hashCode());
        final Object $colortimein = this.getColortimein();
        result = result * 59 + (($colortimein == null) ? 43 : $colortimein.hashCode());
        final Object $colortimout = this.getColortimout();
        result = result * 59 + (($colortimout == null) ? 43 : $colortimout.hashCode());
        result = result * 59 + (this.isCompleteHour() ? 79 : 97);
        result = result * 59 + (this.isLateIn() ? 79 : 97);
        result = result * 59 + (this.isEarlyOut() ? 79 : 97);
        return result;
    }
    
    @Override
    public String toString() {
        return "IOData(date=" + this.getDate() + ", timein=" + this.getTimein() + ", timout=" + this.getTimout() + ", duration=" + this.getDuration() + ", colorduration=" + this.getColorduration() + ", colortimein=" + this.getColortimein() + ", colortimout=" + this.getColortimout() + ", completeHour=" + this.isCompleteHour() + ", lateIn=" + this.isLateIn() + ", earlyOut=" + this.isEarlyOut() + ")";
    }
}
