// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name = "co_staff", schema = "eleave", catalog = "")
public class CoStaffEntity
{
    private int id;
    private String staffid;
    private String title;
    private String name;
    private String status;
    private String ic;
    private String birth;
    private String sex;
    private String marital;
    private String religion;
    private String citizen;
    private String address;
    private String city;
    private String state;
    private String postcode;
    private String phone;
    private String hp;
    private String fax;
    private String email;
    private String pposition;
    private String remarks;
    private String race;
    private String flag;
    private String category;
    private String workertype;
    private String oldic;
    private String datejoin;
    private String pobirth;
    private String department;
    private String position;
    private String location;
    private String imageUrl;
    private String departmentId;
    
    @Id
    @Column(name = "id")
    public int getId() {
        return this.id;
    }
    
    public void setId(final int id) {
        this.id = id;
    }
    
    @Basic
    @Column(name = "staffid")
    public String getStaffid() {
        return this.staffid;
    }
    
    public void setStaffid(final String staffid) {
        this.staffid = staffid;
    }
    
    @Basic
    @Column(name = "title")
    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(final String title) {
        this.title = title;
    }
    
    @Basic
    @Column(name = "name")
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    @Basic
    @Column(name = "status")
    public String getStatus() {
        return this.status;
    }
    
    public void setStatus(final String status) {
        this.status = status;
    }
    
    @Basic
    @Column(name = "ic")
    public String getIc() {
        return this.ic;
    }
    
    public void setIc(final String ic) {
        this.ic = ic;
    }
    
    @Basic
    @Column(name = "birth")
    public String getBirth() {
        return this.birth;
    }
    
    public void setBirth(final String birth) {
        this.birth = birth;
    }
    
    @Basic
    @Column(name = "sex")
    public String getSex() {
        return this.sex;
    }
    
    public void setSex(final String sex) {
        this.sex = sex;
    }
    
    @Basic
    @Column(name = "marital")
    public String getMarital() {
        return this.marital;
    }
    
    public void setMarital(final String marital) {
        this.marital = marital;
    }
    
    @Basic
    @Column(name = "religion")
    public String getReligion() {
        return this.religion;
    }
    
    public void setReligion(final String religion) {
        this.religion = religion;
    }
    
    @Basic
    @Column(name = "citizen")
    public String getCitizen() {
        return this.citizen;
    }
    
    public void setCitizen(final String citizen) {
        this.citizen = citizen;
    }
    
    @Basic
    @Column(name = "address")
    public String getAddress() {
        return this.address;
    }
    
    public void setAddress(final String address) {
        this.address = address;
    }
    
    @Basic
    @Column(name = "city")
    public String getCity() {
        return this.city;
    }
    
    public void setCity(final String city) {
        this.city = city;
    }
    
    @Basic
    @Column(name = "state")
    public String getState() {
        return this.state;
    }
    
    public void setState(final String state) {
        this.state = state;
    }
    
    @Basic
    @Column(name = "postcode")
    public String getPostcode() {
        return this.postcode;
    }
    
    public void setPostcode(final String postcode) {
        this.postcode = postcode;
    }
    
    @Basic
    @Column(name = "phone")
    public String getPhone() {
        return this.phone;
    }
    
    public void setPhone(final String phone) {
        this.phone = phone;
    }
    
    @Basic
    @Column(name = "hp")
    public String getHp() {
        return this.hp;
    }
    
    public void setHp(final String hp) {
        this.hp = hp;
    }
    
    @Basic
    @Column(name = "fax")
    public String getFax() {
        return this.fax;
    }
    
    public void setFax(final String fax) {
        this.fax = fax;
    }
    
    @Basic
    @Column(name = "email")
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(final String email) {
        this.email = email;
    }
    
    @Basic
    @Column(name = "pposition")
    public String getPposition() {
        return this.pposition;
    }
    
    public void setPposition(final String pposition) {
        this.pposition = pposition;
    }
    
    @Basic
    @Column(name = "remarks")
    public String getRemarks() {
        return this.remarks;
    }
    
    public void setRemarks(final String remarks) {
        this.remarks = remarks;
    }
    
    @Basic
    @Column(name = "race")
    public String getRace() {
        return this.race;
    }
    
    public void setRace(final String race) {
        this.race = race;
    }
    
    @Basic
    @Column(name = "flag")
    public String getFlag() {
        return this.flag;
    }
    
    public void setFlag(final String flag) {
        this.flag = flag;
    }
    
    @Basic
    @Column(name = "category")
    public String getCategory() {
        return this.category;
    }
    
    public void setCategory(final String category) {
        this.category = category;
    }
    
    @Basic
    @Column(name = "workertype")
    public String getWorkertype() {
        return this.workertype;
    }
    
    public void setWorkertype(final String workertype) {
        this.workertype = workertype;
    }
    
    @Basic
    @Column(name = "oldic")
    public String getOldic() {
        return this.oldic;
    }
    
    public void setOldic(final String oldic) {
        this.oldic = oldic;
    }
    
    @Basic
    @Column(name = "datejoin")
    public String getDatejoin() {
        return this.datejoin;
    }
    
    public void setDatejoin(final String datejoin) {
        this.datejoin = datejoin;
    }
    
    @Basic
    @Column(name = "pobirth")
    public String getPobirth() {
        return this.pobirth;
    }
    
    public void setPobirth(final String pobirth) {
        this.pobirth = pobirth;
    }
    
    @Basic
    @Column(name = "department")
    public String getDepartment() {
        return this.department;
    }
    
    public void setDepartment(final String department) {
        this.department = department;
    }
    
    @Basic
    @Column(name = "position")
    public String getPosition() {
        return this.position;
    }
    
    public void setPosition(final String position) {
        this.position = position;
    }
    
    @Basic
    @Column(name = "location")
    public String getLocation() {
        return this.location;
    }
    
    public void setLocation(final String location) {
        this.location = location;
    }
    
    @Basic
    @Column(name = "imageURL")
    public String getImageUrl() {
        return this.imageUrl;
    }
    
    public void setImageUrl(final String imageUrl) {
        this.imageUrl = imageUrl;
    }
    
    @Basic
    @Column(name = "departmentID")
    public String getDepartmentId() {
        return this.departmentId;
    }
    
    public void setDepartmentId(final String departmentId) {
        this.departmentId = departmentId;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final CoStaffEntity that = (CoStaffEntity)o;
        if (this.id != that.id) {
            return false;
        }
        Label_0075: {
            if (this.staffid != null) {
                if (this.staffid.equals(that.staffid)) {
                    break Label_0075;
                }
            }
            else if (that.staffid == null) {
                break Label_0075;
            }
            return false;
        }
        Label_0108: {
            if (this.title != null) {
                if (this.title.equals(that.title)) {
                    break Label_0108;
                }
            }
            else if (that.title == null) {
                break Label_0108;
            }
            return false;
        }
        Label_0141: {
            if (this.name != null) {
                if (this.name.equals(that.name)) {
                    break Label_0141;
                }
            }
            else if (that.name == null) {
                break Label_0141;
            }
            return false;
        }
        Label_0174: {
            if (this.status != null) {
                if (this.status.equals(that.status)) {
                    break Label_0174;
                }
            }
            else if (that.status == null) {
                break Label_0174;
            }
            return false;
        }
        Label_0207: {
            if (this.ic != null) {
                if (this.ic.equals(that.ic)) {
                    break Label_0207;
                }
            }
            else if (that.ic == null) {
                break Label_0207;
            }
            return false;
        }
        Label_0240: {
            if (this.birth != null) {
                if (this.birth.equals(that.birth)) {
                    break Label_0240;
                }
            }
            else if (that.birth == null) {
                break Label_0240;
            }
            return false;
        }
        Label_0273: {
            if (this.sex != null) {
                if (this.sex.equals(that.sex)) {
                    break Label_0273;
                }
            }
            else if (that.sex == null) {
                break Label_0273;
            }
            return false;
        }
        Label_0306: {
            if (this.marital != null) {
                if (this.marital.equals(that.marital)) {
                    break Label_0306;
                }
            }
            else if (that.marital == null) {
                break Label_0306;
            }
            return false;
        }
        Label_0339: {
            if (this.religion != null) {
                if (this.religion.equals(that.religion)) {
                    break Label_0339;
                }
            }
            else if (that.religion == null) {
                break Label_0339;
            }
            return false;
        }
        Label_0372: {
            if (this.citizen != null) {
                if (this.citizen.equals(that.citizen)) {
                    break Label_0372;
                }
            }
            else if (that.citizen == null) {
                break Label_0372;
            }
            return false;
        }
        Label_0405: {
            if (this.address != null) {
                if (this.address.equals(that.address)) {
                    break Label_0405;
                }
            }
            else if (that.address == null) {
                break Label_0405;
            }
            return false;
        }
        Label_0438: {
            if (this.city != null) {
                if (this.city.equals(that.city)) {
                    break Label_0438;
                }
            }
            else if (that.city == null) {
                break Label_0438;
            }
            return false;
        }
        Label_0471: {
            if (this.state != null) {
                if (this.state.equals(that.state)) {
                    break Label_0471;
                }
            }
            else if (that.state == null) {
                break Label_0471;
            }
            return false;
        }
        Label_0504: {
            if (this.postcode != null) {
                if (this.postcode.equals(that.postcode)) {
                    break Label_0504;
                }
            }
            else if (that.postcode == null) {
                break Label_0504;
            }
            return false;
        }
        Label_0537: {
            if (this.phone != null) {
                if (this.phone.equals(that.phone)) {
                    break Label_0537;
                }
            }
            else if (that.phone == null) {
                break Label_0537;
            }
            return false;
        }
        Label_0570: {
            if (this.hp != null) {
                if (this.hp.equals(that.hp)) {
                    break Label_0570;
                }
            }
            else if (that.hp == null) {
                break Label_0570;
            }
            return false;
        }
        Label_0603: {
            if (this.fax != null) {
                if (this.fax.equals(that.fax)) {
                    break Label_0603;
                }
            }
            else if (that.fax == null) {
                break Label_0603;
            }
            return false;
        }
        Label_0636: {
            if (this.email != null) {
                if (this.email.equals(that.email)) {
                    break Label_0636;
                }
            }
            else if (that.email == null) {
                break Label_0636;
            }
            return false;
        }
        Label_0669: {
            if (this.pposition != null) {
                if (this.pposition.equals(that.pposition)) {
                    break Label_0669;
                }
            }
            else if (that.pposition == null) {
                break Label_0669;
            }
            return false;
        }
        Label_0702: {
            if (this.remarks != null) {
                if (this.remarks.equals(that.remarks)) {
                    break Label_0702;
                }
            }
            else if (that.remarks == null) {
                break Label_0702;
            }
            return false;
        }
        Label_0735: {
            if (this.race != null) {
                if (this.race.equals(that.race)) {
                    break Label_0735;
                }
            }
            else if (that.race == null) {
                break Label_0735;
            }
            return false;
        }
        Label_0768: {
            if (this.flag != null) {
                if (this.flag.equals(that.flag)) {
                    break Label_0768;
                }
            }
            else if (that.flag == null) {
                break Label_0768;
            }
            return false;
        }
        Label_0801: {
            if (this.category != null) {
                if (this.category.equals(that.category)) {
                    break Label_0801;
                }
            }
            else if (that.category == null) {
                break Label_0801;
            }
            return false;
        }
        Label_0834: {
            if (this.workertype != null) {
                if (this.workertype.equals(that.workertype)) {
                    break Label_0834;
                }
            }
            else if (that.workertype == null) {
                break Label_0834;
            }
            return false;
        }
        Label_0867: {
            if (this.oldic != null) {
                if (this.oldic.equals(that.oldic)) {
                    break Label_0867;
                }
            }
            else if (that.oldic == null) {
                break Label_0867;
            }
            return false;
        }
        Label_0900: {
            if (this.datejoin != null) {
                if (this.datejoin.equals(that.datejoin)) {
                    break Label_0900;
                }
            }
            else if (that.datejoin == null) {
                break Label_0900;
            }
            return false;
        }
        Label_0933: {
            if (this.pobirth != null) {
                if (this.pobirth.equals(that.pobirth)) {
                    break Label_0933;
                }
            }
            else if (that.pobirth == null) {
                break Label_0933;
            }
            return false;
        }
        Label_0966: {
            if (this.department != null) {
                if (this.department.equals(that.department)) {
                    break Label_0966;
                }
            }
            else if (that.department == null) {
                break Label_0966;
            }
            return false;
        }
        Label_0999: {
            if (this.position != null) {
                if (this.position.equals(that.position)) {
                    break Label_0999;
                }
            }
            else if (that.position == null) {
                break Label_0999;
            }
            return false;
        }
        Label_1032: {
            if (this.location != null) {
                if (this.location.equals(that.location)) {
                    break Label_1032;
                }
            }
            else if (that.location == null) {
                break Label_1032;
            }
            return false;
        }
        Label_1065: {
            if (this.imageUrl != null) {
                if (this.imageUrl.equals(that.imageUrl)) {
                    break Label_1065;
                }
            }
            else if (that.imageUrl == null) {
                break Label_1065;
            }
            return false;
        }
        if (this.departmentId != null) {
            if (this.departmentId.equals(that.departmentId)) {
                return true;
            }
        }
        else if (that.departmentId == null) {
            return true;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        int result = this.id;
        result = 31 * result + ((this.staffid != null) ? this.staffid.hashCode() : 0);
        result = 31 * result + ((this.title != null) ? this.title.hashCode() : 0);
        result = 31 * result + ((this.name != null) ? this.name.hashCode() : 0);
        result = 31 * result + ((this.status != null) ? this.status.hashCode() : 0);
        result = 31 * result + ((this.ic != null) ? this.ic.hashCode() : 0);
        result = 31 * result + ((this.birth != null) ? this.birth.hashCode() : 0);
        result = 31 * result + ((this.sex != null) ? this.sex.hashCode() : 0);
        result = 31 * result + ((this.marital != null) ? this.marital.hashCode() : 0);
        result = 31 * result + ((this.religion != null) ? this.religion.hashCode() : 0);
        result = 31 * result + ((this.citizen != null) ? this.citizen.hashCode() : 0);
        result = 31 * result + ((this.address != null) ? this.address.hashCode() : 0);
        result = 31 * result + ((this.city != null) ? this.city.hashCode() : 0);
        result = 31 * result + ((this.state != null) ? this.state.hashCode() : 0);
        result = 31 * result + ((this.postcode != null) ? this.postcode.hashCode() : 0);
        result = 31 * result + ((this.phone != null) ? this.phone.hashCode() : 0);
        result = 31 * result + ((this.hp != null) ? this.hp.hashCode() : 0);
        result = 31 * result + ((this.fax != null) ? this.fax.hashCode() : 0);
        result = 31 * result + ((this.email != null) ? this.email.hashCode() : 0);
        result = 31 * result + ((this.pposition != null) ? this.pposition.hashCode() : 0);
        result = 31 * result + ((this.remarks != null) ? this.remarks.hashCode() : 0);
        result = 31 * result + ((this.race != null) ? this.race.hashCode() : 0);
        result = 31 * result + ((this.flag != null) ? this.flag.hashCode() : 0);
        result = 31 * result + ((this.category != null) ? this.category.hashCode() : 0);
        result = 31 * result + ((this.workertype != null) ? this.workertype.hashCode() : 0);
        result = 31 * result + ((this.oldic != null) ? this.oldic.hashCode() : 0);
        result = 31 * result + ((this.datejoin != null) ? this.datejoin.hashCode() : 0);
        result = 31 * result + ((this.pobirth != null) ? this.pobirth.hashCode() : 0);
        result = 31 * result + ((this.department != null) ? this.department.hashCode() : 0);
        result = 31 * result + ((this.position != null) ? this.position.hashCode() : 0);
        result = 31 * result + ((this.location != null) ? this.location.hashCode() : 0);
        result = 31 * result + ((this.imageUrl != null) ? this.imageUrl.hashCode() : 0);
        result = 31 * result + ((this.departmentId != null) ? this.departmentId.hashCode() : 0);
        return result;
    }
}
