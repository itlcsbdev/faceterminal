// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.model;

public class TempData
{
    private int slot_card_records_id;
    private int physical_id;
    private int person_id;
    private int person_btype;
    private int card_read_id;
    private String card_number;
    private int event_type;
    private String record_storage_key;
    private int auth_result;
    private String swip_card_time;
    private String update_time;
    private String create_time;
    private int is_face_terminal_event;
    private String is_deleted;
    private int is_cleared;
    private int is_allow_cleared;
    private String swip_card_rev_time;
    private int time_offset;
    private int attendance_status;
    private int source_type;
    private String snap_pic_url;
    private String temp_data;
    private int temp_status;
    private int mask_status;
    
    public int getSlot_card_records_id() {
        return this.slot_card_records_id;
    }
    
    public int getPhysical_id() {
        return this.physical_id;
    }
    
    public int getPerson_id() {
        return this.person_id;
    }
    
    public int getPerson_btype() {
        return this.person_btype;
    }
    
    public int getCard_read_id() {
        return this.card_read_id;
    }
    
    public String getCard_number() {
        return this.card_number;
    }
    
    public int getEvent_type() {
        return this.event_type;
    }
    
    public String getRecord_storage_key() {
        return this.record_storage_key;
    }
    
    public int getAuth_result() {
        return this.auth_result;
    }
    
    public String getSwip_card_time() {
        return this.swip_card_time;
    }
    
    public String getUpdate_time() {
        return this.update_time;
    }
    
    public String getCreate_time() {
        return this.create_time;
    }
    
    public int getIs_face_terminal_event() {
        return this.is_face_terminal_event;
    }
    
    public String getIs_deleted() {
        return this.is_deleted;
    }
    
    public int getIs_cleared() {
        return this.is_cleared;
    }
    
    public int getIs_allow_cleared() {
        return this.is_allow_cleared;
    }
    
    public String getSwip_card_rev_time() {
        return this.swip_card_rev_time;
    }
    
    public int getTime_offset() {
        return this.time_offset;
    }
    
    public int getAttendance_status() {
        return this.attendance_status;
    }
    
    public int getSource_type() {
        return this.source_type;
    }
    
    public String getSnap_pic_url() {
        return this.snap_pic_url;
    }
    
    public String getTemp_data() {
        return this.temp_data;
    }
    
    public int getTemp_status() {
        return this.temp_status;
    }
    
    public int getMask_status() {
        return this.mask_status;
    }
    
    public void setSlot_card_records_id(final int slot_card_records_id) {
        this.slot_card_records_id = slot_card_records_id;
    }
    
    public void setPhysical_id(final int physical_id) {
        this.physical_id = physical_id;
    }
    
    public void setPerson_id(final int person_id) {
        this.person_id = person_id;
    }
    
    public void setPerson_btype(final int person_btype) {
        this.person_btype = person_btype;
    }
    
    public void setCard_read_id(final int card_read_id) {
        this.card_read_id = card_read_id;
    }
    
    public void setCard_number(final String card_number) {
        this.card_number = card_number;
    }
    
    public void setEvent_type(final int event_type) {
        this.event_type = event_type;
    }
    
    public void setRecord_storage_key(final String record_storage_key) {
        this.record_storage_key = record_storage_key;
    }
    
    public void setAuth_result(final int auth_result) {
        this.auth_result = auth_result;
    }
    
    public void setSwip_card_time(final String swip_card_time) {
        this.swip_card_time = swip_card_time;
    }
    
    public void setUpdate_time(final String update_time) {
        this.update_time = update_time;
    }
    
    public void setCreate_time(final String create_time) {
        this.create_time = create_time;
    }
    
    public void setIs_face_terminal_event(final int is_face_terminal_event) {
        this.is_face_terminal_event = is_face_terminal_event;
    }
    
    public void setIs_deleted(final String is_deleted) {
        this.is_deleted = is_deleted;
    }
    
    public void setIs_cleared(final int is_cleared) {
        this.is_cleared = is_cleared;
    }
    
    public void setIs_allow_cleared(final int is_allow_cleared) {
        this.is_allow_cleared = is_allow_cleared;
    }
    
    public void setSwip_card_rev_time(final String swip_card_rev_time) {
        this.swip_card_rev_time = swip_card_rev_time;
    }
    
    public void setTime_offset(final int time_offset) {
        this.time_offset = time_offset;
    }
    
    public void setAttendance_status(final int attendance_status) {
        this.attendance_status = attendance_status;
    }
    
    public void setSource_type(final int source_type) {
        this.source_type = source_type;
    }
    
    public void setSnap_pic_url(final String snap_pic_url) {
        this.snap_pic_url = snap_pic_url;
    }
    
    public void setTemp_data(final String temp_data) {
        this.temp_data = temp_data;
    }
    
    public void setTemp_status(final int temp_status) {
        this.temp_status = temp_status;
    }
    
    public void setMask_status(final int mask_status) {
        this.mask_status = mask_status;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof TempData)) {
            return false;
        }
        final TempData other = (TempData)o;
        if (!other.canEqual((Object)this)) {
            return false;
        }
        if (this.getSlot_card_records_id() != other.getSlot_card_records_id()) {
            return false;
        }
        if (this.getPhysical_id() != other.getPhysical_id()) {
            return false;
        }
        if (this.getPerson_id() != other.getPerson_id()) {
            return false;
        }
        if (this.getPerson_btype() != other.getPerson_btype()) {
            return false;
        }
        if (this.getCard_read_id() != other.getCard_read_id()) {
            return false;
        }
        final Object this$card_number = this.getCard_number();
        final Object other$card_number = other.getCard_number();
        Label_0130: {
            if (this$card_number == null) {
                if (other$card_number == null) {
                    break Label_0130;
                }
            }
            else if (this$card_number.equals(other$card_number)) {
                break Label_0130;
            }
            return false;
        }
        if (this.getEvent_type() != other.getEvent_type()) {
            return false;
        }
        final Object this$record_storage_key = this.getRecord_storage_key();
        final Object other$record_storage_key = other.getRecord_storage_key();
        Label_0180: {
            if (this$record_storage_key == null) {
                if (other$record_storage_key == null) {
                    break Label_0180;
                }
            }
            else if (this$record_storage_key.equals(other$record_storage_key)) {
                break Label_0180;
            }
            return false;
        }
        if (this.getAuth_result() != other.getAuth_result()) {
            return false;
        }
        final Object this$swip_card_time = this.getSwip_card_time();
        final Object other$swip_card_time = other.getSwip_card_time();
        Label_0230: {
            if (this$swip_card_time == null) {
                if (other$swip_card_time == null) {
                    break Label_0230;
                }
            }
            else if (this$swip_card_time.equals(other$swip_card_time)) {
                break Label_0230;
            }
            return false;
        }
        final Object this$update_time = this.getUpdate_time();
        final Object other$update_time = other.getUpdate_time();
        Label_0267: {
            if (this$update_time == null) {
                if (other$update_time == null) {
                    break Label_0267;
                }
            }
            else if (this$update_time.equals(other$update_time)) {
                break Label_0267;
            }
            return false;
        }
        final Object this$create_time = this.getCreate_time();
        final Object other$create_time = other.getCreate_time();
        Label_0304: {
            if (this$create_time == null) {
                if (other$create_time == null) {
                    break Label_0304;
                }
            }
            else if (this$create_time.equals(other$create_time)) {
                break Label_0304;
            }
            return false;
        }
        if (this.getIs_face_terminal_event() != other.getIs_face_terminal_event()) {
            return false;
        }
        final Object this$is_deleted = this.getIs_deleted();
        final Object other$is_deleted = other.getIs_deleted();
        Label_0354: {
            if (this$is_deleted == null) {
                if (other$is_deleted == null) {
                    break Label_0354;
                }
            }
            else if (this$is_deleted.equals(other$is_deleted)) {
                break Label_0354;
            }
            return false;
        }
        if (this.getIs_cleared() != other.getIs_cleared()) {
            return false;
        }
        if (this.getIs_allow_cleared() != other.getIs_allow_cleared()) {
            return false;
        }
        final Object this$swip_card_rev_time = this.getSwip_card_rev_time();
        final Object other$swip_card_rev_time = other.getSwip_card_rev_time();
        Label_0417: {
            if (this$swip_card_rev_time == null) {
                if (other$swip_card_rev_time == null) {
                    break Label_0417;
                }
            }
            else if (this$swip_card_rev_time.equals(other$swip_card_rev_time)) {
                break Label_0417;
            }
            return false;
        }
        if (this.getTime_offset() != other.getTime_offset()) {
            return false;
        }
        if (this.getAttendance_status() != other.getAttendance_status()) {
            return false;
        }
        if (this.getSource_type() != other.getSource_type()) {
            return false;
        }
        final Object this$snap_pic_url = this.getSnap_pic_url();
        final Object other$snap_pic_url = other.getSnap_pic_url();
        Label_0493: {
            if (this$snap_pic_url == null) {
                if (other$snap_pic_url == null) {
                    break Label_0493;
                }
            }
            else if (this$snap_pic_url.equals(other$snap_pic_url)) {
                break Label_0493;
            }
            return false;
        }
        final Object this$temp_data = this.getTemp_data();
        final Object other$temp_data = other.getTemp_data();
        if (this$temp_data == null) {
            if (other$temp_data == null) {
                return this.getTemp_status() == other.getTemp_status() && this.getMask_status() == other.getMask_status();
            }
        }
        else if (this$temp_data.equals(other$temp_data)) {
            return this.getTemp_status() == other.getTemp_status() && this.getMask_status() == other.getMask_status();
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof TempData;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        result = result * 59 + this.getSlot_card_records_id();
        result = result * 59 + this.getPhysical_id();
        result = result * 59 + this.getPerson_id();
        result = result * 59 + this.getPerson_btype();
        result = result * 59 + this.getCard_read_id();
        final Object $card_number = this.getCard_number();
        result = result * 59 + (($card_number == null) ? 43 : $card_number.hashCode());
        result = result * 59 + this.getEvent_type();
        final Object $record_storage_key = this.getRecord_storage_key();
        result = result * 59 + (($record_storage_key == null) ? 43 : $record_storage_key.hashCode());
        result = result * 59 + this.getAuth_result();
        final Object $swip_card_time = this.getSwip_card_time();
        result = result * 59 + (($swip_card_time == null) ? 43 : $swip_card_time.hashCode());
        final Object $update_time = this.getUpdate_time();
        result = result * 59 + (($update_time == null) ? 43 : $update_time.hashCode());
        final Object $create_time = this.getCreate_time();
        result = result * 59 + (($create_time == null) ? 43 : $create_time.hashCode());
        result = result * 59 + this.getIs_face_terminal_event();
        final Object $is_deleted = this.getIs_deleted();
        result = result * 59 + (($is_deleted == null) ? 43 : $is_deleted.hashCode());
        result = result * 59 + this.getIs_cleared();
        result = result * 59 + this.getIs_allow_cleared();
        final Object $swip_card_rev_time = this.getSwip_card_rev_time();
        result = result * 59 + (($swip_card_rev_time == null) ? 43 : $swip_card_rev_time.hashCode());
        result = result * 59 + this.getTime_offset();
        result = result * 59 + this.getAttendance_status();
        result = result * 59 + this.getSource_type();
        final Object $snap_pic_url = this.getSnap_pic_url();
        result = result * 59 + (($snap_pic_url == null) ? 43 : $snap_pic_url.hashCode());
        final Object $temp_data = this.getTemp_data();
        result = result * 59 + (($temp_data == null) ? 43 : $temp_data.hashCode());
        result = result * 59 + this.getTemp_status();
        result = result * 59 + this.getMask_status();
        return result;
    }
    
    @Override
    public String toString() {
        return "TempData(slot_card_records_id=" + this.getSlot_card_records_id() + ", physical_id=" + this.getPhysical_id() + ", person_id=" + this.getPerson_id() + ", person_btype=" + this.getPerson_btype() + ", card_read_id=" + this.getCard_read_id() + ", card_number=" + this.getCard_number() + ", event_type=" + this.getEvent_type() + ", record_storage_key=" + this.getRecord_storage_key() + ", auth_result=" + this.getAuth_result() + ", swip_card_time=" + this.getSwip_card_time() + ", update_time=" + this.getUpdate_time() + ", create_time=" + this.getCreate_time() + ", is_face_terminal_event=" + this.getIs_face_terminal_event() + ", is_deleted=" + this.getIs_deleted() + ", is_cleared=" + this.getIs_cleared() + ", is_allow_cleared=" + this.getIs_allow_cleared() + ", swip_card_rev_time=" + this.getSwip_card_rev_time() + ", time_offset=" + this.getTime_offset() + ", attendance_status=" + this.getAttendance_status() + ", source_type=" + this.getSource_type() + ", snap_pic_url=" + this.getSnap_pic_url() + ", temp_data=" + this.getTemp_data() + ", temp_status=" + this.getTemp_status() + ", mask_status=" + this.getMask_status() + ")";
    }
}
