// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name = "leave_attachment", schema = "eleave", catalog = "")
public class LeaveAttachmentEntity
{
    private int id;
    private String leaveId;
    private String filename;
    private String staffId;
    private String sessionId;
    
    @Id
    @Column(name = "id")
    public int getId() {
        return this.id;
    }
    
    public void setId(final int id) {
        this.id = id;
    }
    
    @Basic
    @Column(name = "leaveID")
    public String getLeaveId() {
        return this.leaveId;
    }
    
    public void setLeaveId(final String leaveId) {
        this.leaveId = leaveId;
    }
    
    @Basic
    @Column(name = "filename")
    public String getFilename() {
        return this.filename;
    }
    
    public void setFilename(final String filename) {
        this.filename = filename;
    }
    
    @Basic
    @Column(name = "staffID")
    public String getStaffId() {
        return this.staffId;
    }
    
    public void setStaffId(final String staffId) {
        this.staffId = staffId;
    }
    
    @Basic
    @Column(name = "sessionID")
    public String getSessionId() {
        return this.sessionId;
    }
    
    public void setSessionId(final String sessionId) {
        this.sessionId = sessionId;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final LeaveAttachmentEntity that = (LeaveAttachmentEntity)o;
        if (this.id != that.id) {
            return false;
        }
        Label_0075: {
            if (this.leaveId != null) {
                if (this.leaveId.equals(that.leaveId)) {
                    break Label_0075;
                }
            }
            else if (that.leaveId == null) {
                break Label_0075;
            }
            return false;
        }
        Label_0108: {
            if (this.filename != null) {
                if (this.filename.equals(that.filename)) {
                    break Label_0108;
                }
            }
            else if (that.filename == null) {
                break Label_0108;
            }
            return false;
        }
        Label_0141: {
            if (this.staffId != null) {
                if (this.staffId.equals(that.staffId)) {
                    break Label_0141;
                }
            }
            else if (that.staffId == null) {
                break Label_0141;
            }
            return false;
        }
        if (this.sessionId != null) {
            if (this.sessionId.equals(that.sessionId)) {
                return true;
            }
        }
        else if (that.sessionId == null) {
            return true;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        int result = this.id;
        result = 31 * result + ((this.leaveId != null) ? this.leaveId.hashCode() : 0);
        result = 31 * result + ((this.filename != null) ? this.filename.hashCode() : 0);
        result = 31 * result + ((this.staffId != null) ? this.staffId.hashCode() : 0);
        result = 31 * result + ((this.sessionId != null) ? this.sessionId.hashCode() : 0);
        return result;
    }
}
