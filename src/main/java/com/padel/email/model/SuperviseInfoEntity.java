// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name = "supervise_info", schema = "eleave", catalog = "")
public class SuperviseInfoEntity
{
    private int id;
    private String staffId;
    private String supervisorId;
    private String headId;
    
    @Id
    @Column(name = "id")
    public int getId() {
        return this.id;
    }
    
    public void setId(final int id) {
        this.id = id;
    }
    
    @Basic
    @Column(name = "staffID")
    public String getStaffId() {
        return this.staffId;
    }
    
    public void setStaffId(final String staffId) {
        this.staffId = staffId;
    }
    
    @Basic
    @Column(name = "supervisorID")
    public String getSupervisorId() {
        return this.supervisorId;
    }
    
    public void setSupervisorId(final String supervisorId) {
        this.supervisorId = supervisorId;
    }
    
    @Basic
    @Column(name = "headID")
    public String getHeadId() {
        return this.headId;
    }
    
    public void setHeadId(final String headId) {
        this.headId = headId;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final SuperviseInfoEntity that = (SuperviseInfoEntity)o;
        if (this.id != that.id) {
            return false;
        }
        Label_0075: {
            if (this.staffId != null) {
                if (this.staffId.equals(that.staffId)) {
                    break Label_0075;
                }
            }
            else if (that.staffId == null) {
                break Label_0075;
            }
            return false;
        }
        Label_0108: {
            if (this.supervisorId != null) {
                if (this.supervisorId.equals(that.supervisorId)) {
                    break Label_0108;
                }
            }
            else if (that.supervisorId == null) {
                break Label_0108;
            }
            return false;
        }
        if (this.headId != null) {
            if (this.headId.equals(that.headId)) {
                return true;
            }
        }
        else if (that.headId == null) {
            return true;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        int result = this.id;
        result = 31 * result + ((this.staffId != null) ? this.staffId.hashCode() : 0);
        result = 31 * result + ((this.supervisorId != null) ? this.supervisorId.hashCode() : 0);
        result = 31 * result + ((this.headId != null) ? this.headId.hashCode() : 0);
        return result;
    }
}
