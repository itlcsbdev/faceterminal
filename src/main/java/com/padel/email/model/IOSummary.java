// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.model;

import java.util.List;

public class IOSummary
{
    private List<IOData> io;
    
    public List<IOData> getIo() {
        return this.io;
    }
    
    public void setIo(final List<IOData> io) {
        this.io = io;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof IOSummary)) {
            return false;
        }
        final IOSummary other = (IOSummary)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$io = this.getIo();
        final Object other$io = other.getIo();
        if (this$io == null) {
            if (other$io == null) {
                return true;
            }
        }
        else if (this$io.equals(other$io)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof IOSummary;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $io = this.getIo();
        result = result * 59 + (($io == null) ? 43 : $io.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "IOSummary(io=" + this.getIo() + ")";
    }
}
