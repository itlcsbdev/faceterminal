// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name = "user_access", schema = "eleave", catalog = "")
public class UserAccessEntity
{
    private int id;
    private String staffId;
    private int level;
    
    @Id
    @Column(name = "id")
    public int getId() {
        return this.id;
    }
    
    public void setId(final int id) {
        this.id = id;
    }
    
    @Basic
    @Column(name = "staffID")
    public String getStaffId() {
        return this.staffId;
    }
    
    public void setStaffId(final String staffId) {
        this.staffId = staffId;
    }
    
    @Basic
    @Column(name = "level")
    public int getLevel() {
        return this.level;
    }
    
    public void setLevel(final int level) {
        this.level = level;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final UserAccessEntity that = (UserAccessEntity)o;
        if (this.id != that.id) {
            return false;
        }
        if (this.level != that.level) {
            return false;
        }
        if (this.staffId != null) {
            if (this.staffId.equals(that.staffId)) {
                return true;
            }
        }
        else if (that.staffId == null) {
            return true;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        int result = this.id;
        result = 31 * result + ((this.staffId != null) ? this.staffId.hashCode() : 0);
        result = 31 * result + this.level;
        return result;
    }
}
