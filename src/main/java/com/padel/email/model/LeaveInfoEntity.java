// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name = "leave_info", schema = "eleave", catalog = "")
public class LeaveInfoEntity
{
    private int id;
    private String staffId;
    private Integer eligibleleave;
    private String year;
    private Integer bf;
    private Integer mc;
    
    @Id
    @Column(name = "id")
    public int getId() {
        return this.id;
    }
    
    public void setId(final int id) {
        this.id = id;
    }
    
    @Basic
    @Column(name = "staffID")
    public String getStaffId() {
        return this.staffId;
    }
    
    public void setStaffId(final String staffId) {
        this.staffId = staffId;
    }
    
    @Basic
    @Column(name = "eligibleleave")
    public Integer getEligibleleave() {
        return this.eligibleleave;
    }
    
    public void setEligibleleave(final Integer eligibleleave) {
        this.eligibleleave = eligibleleave;
    }
    
    @Basic
    @Column(name = "year")
    public String getYear() {
        return this.year;
    }
    
    public void setYear(final String year) {
        this.year = year;
    }
    
    @Basic
    @Column(name = "bf")
    public Integer getBf() {
        return this.bf;
    }
    
    public void setBf(final Integer bf) {
        this.bf = bf;
    }
    
    @Basic
    @Column(name = "mc")
    public Integer getMc() {
        return this.mc;
    }
    
    public void setMc(final Integer mc) {
        this.mc = mc;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final LeaveInfoEntity that = (LeaveInfoEntity)o;
        if (this.id != that.id) {
            return false;
        }
        Label_0075: {
            if (this.staffId != null) {
                if (this.staffId.equals(that.staffId)) {
                    break Label_0075;
                }
            }
            else if (that.staffId == null) {
                break Label_0075;
            }
            return false;
        }
        Label_0108: {
            if (this.eligibleleave != null) {
                if (this.eligibleleave.equals(that.eligibleleave)) {
                    break Label_0108;
                }
            }
            else if (that.eligibleleave == null) {
                break Label_0108;
            }
            return false;
        }
        Label_0141: {
            if (this.year != null) {
                if (this.year.equals(that.year)) {
                    break Label_0141;
                }
            }
            else if (that.year == null) {
                break Label_0141;
            }
            return false;
        }
        Label_0174: {
            if (this.bf != null) {
                if (this.bf.equals(that.bf)) {
                    break Label_0174;
                }
            }
            else if (that.bf == null) {
                break Label_0174;
            }
            return false;
        }
        if (this.mc != null) {
            if (this.mc.equals(that.mc)) {
                return true;
            }
        }
        else if (that.mc == null) {
            return true;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        int result = this.id;
        result = 31 * result + ((this.staffId != null) ? this.staffId.hashCode() : 0);
        result = 31 * result + ((this.eligibleleave != null) ? this.eligibleleave.hashCode() : 0);
        result = 31 * result + ((this.year != null) ? this.year.hashCode() : 0);
        result = 31 * result + ((this.bf != null) ? this.bf.hashCode() : 0);
        result = 31 * result + ((this.mc != null) ? this.mc.hashCode() : 0);
        return result;
    }
}
