// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.model;

public class FaceResult
{
    private String table;
    private String action;
    private TempData data;
    
    public String getTable() {
        return this.table;
    }
    
    public String getAction() {
        return this.action;
    }
    
    public TempData getData() {
        return this.data;
    }
    
    public void setTable(final String table) {
        this.table = table;
    }
    
    public void setAction(final String action) {
        this.action = action;
    }
    
    public void setData(final TempData data) {
        this.data = data;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof FaceResult)) {
            return false;
        }
        final FaceResult other = (FaceResult)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$table = this.getTable();
        final Object other$table = other.getTable();
        Label_0065: {
            if (this$table == null) {
                if (other$table == null) {
                    break Label_0065;
                }
            }
            else if (this$table.equals(other$table)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$action = this.getAction();
        final Object other$action = other.getAction();
        Label_0102: {
            if (this$action == null) {
                if (other$action == null) {
                    break Label_0102;
                }
            }
            else if (this$action.equals(other$action)) {
                break Label_0102;
            }
            return false;
        }
        final Object this$data = this.getData();
        final Object other$data = other.getData();
        if (this$data == null) {
            if (other$data == null) {
                return true;
            }
        }
        else if (this$data.equals(other$data)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof FaceResult;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $table = this.getTable();
        result = result * 59 + (($table == null) ? 43 : $table.hashCode());
        final Object $action = this.getAction();
        result = result * 59 + (($action == null) ? 43 : $action.hashCode());
        final Object $data = this.getData();
        result = result * 59 + (($data == null) ? 43 : $data.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "FaceResult(table=" + this.getTable() + ", action=" + this.getAction() + ", data=" + this.getData() + ")";
    }
}
