// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name = "leave_master", schema = "eleave", catalog = "")
public class LeaveMasterEntity
{
    private String leaveId;
    private String staffId;
    private String type;
    private String reason;
    private Integer days;
    private String dateapply;
    private String datestart;
    private String dateend;
    private String supervisorId;
    private String supervisorDate;
    private String headId;
    private String headDate;
    private String hrId;
    private String hrDate;
    private String checkId;
    private String checkDate;
    private String status;
    private String year;
    private String period;
    private Integer stafflevel;
    
    @Id
    @Column(name = "leaveID")
    public String getLeaveId() {
        return this.leaveId;
    }
    
    public void setLeaveId(final String leaveId) {
        this.leaveId = leaveId;
    }
    
    @Basic
    @Column(name = "staffID")
    public String getStaffId() {
        return this.staffId;
    }
    
    public void setStaffId(final String staffId) {
        this.staffId = staffId;
    }
    
    @Basic
    @Column(name = "type")
    public String getType() {
        return this.type;
    }
    
    public void setType(final String type) {
        this.type = type;
    }
    
    @Basic
    @Column(name = "reason")
    public String getReason() {
        return this.reason;
    }
    
    public void setReason(final String reason) {
        this.reason = reason;
    }
    
    @Basic
    @Column(name = "days")
    public Integer getDays() {
        return this.days;
    }
    
    public void setDays(final Integer days) {
        this.days = days;
    }
    
    @Basic
    @Column(name = "dateapply")
    public String getDateapply() {
        return this.dateapply;
    }
    
    public void setDateapply(final String dateapply) {
        this.dateapply = dateapply;
    }
    
    @Basic
    @Column(name = "datestart")
    public String getDatestart() {
        return this.datestart;
    }
    
    public void setDatestart(final String datestart) {
        this.datestart = datestart;
    }
    
    @Basic
    @Column(name = "dateend")
    public String getDateend() {
        return this.dateend;
    }
    
    public void setDateend(final String dateend) {
        this.dateend = dateend;
    }
    
    @Basic
    @Column(name = "supervisorID")
    public String getSupervisorId() {
        return this.supervisorId;
    }
    
    public void setSupervisorId(final String supervisorId) {
        this.supervisorId = supervisorId;
    }
    
    @Basic
    @Column(name = "supervisor_date")
    public String getSupervisorDate() {
        return this.supervisorDate;
    }
    
    public void setSupervisorDate(final String supervisorDate) {
        this.supervisorDate = supervisorDate;
    }
    
    @Basic
    @Column(name = "headID")
    public String getHeadId() {
        return this.headId;
    }
    
    public void setHeadId(final String headId) {
        this.headId = headId;
    }
    
    @Basic
    @Column(name = "head_date")
    public String getHeadDate() {
        return this.headDate;
    }
    
    public void setHeadDate(final String headDate) {
        this.headDate = headDate;
    }
    
    @Basic
    @Column(name = "hrID")
    public String getHrId() {
        return this.hrId;
    }
    
    public void setHrId(final String hrId) {
        this.hrId = hrId;
    }
    
    @Basic
    @Column(name = "hr_date")
    public String getHrDate() {
        return this.hrDate;
    }
    
    public void setHrDate(final String hrDate) {
        this.hrDate = hrDate;
    }
    
    @Basic
    @Column(name = "checkID")
    public String getCheckId() {
        return this.checkId;
    }
    
    public void setCheckId(final String checkId) {
        this.checkId = checkId;
    }
    
    @Basic
    @Column(name = "check_date")
    public String getCheckDate() {
        return this.checkDate;
    }
    
    public void setCheckDate(final String checkDate) {
        this.checkDate = checkDate;
    }
    
    @Basic
    @Column(name = "status")
    public String getStatus() {
        return this.status;
    }
    
    public void setStatus(final String status) {
        this.status = status;
    }
    
    @Basic
    @Column(name = "year")
    public String getYear() {
        return this.year;
    }
    
    public void setYear(final String year) {
        this.year = year;
    }
    
    @Basic
    @Column(name = "period")
    public String getPeriod() {
        return this.period;
    }
    
    public void setPeriod(final String period) {
        this.period = period;
    }
    
    @Basic
    @Column(name = "stafflevel")
    public Integer getStafflevel() {
        return this.stafflevel;
    }
    
    public void setStafflevel(final Integer stafflevel) {
        this.stafflevel = stafflevel;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final LeaveMasterEntity that = (LeaveMasterEntity)o;
        Label_0062: {
            if (this.leaveId != null) {
                if (this.leaveId.equals(that.leaveId)) {
                    break Label_0062;
                }
            }
            else if (that.leaveId == null) {
                break Label_0062;
            }
            return false;
        }
        Label_0095: {
            if (this.staffId != null) {
                if (this.staffId.equals(that.staffId)) {
                    break Label_0095;
                }
            }
            else if (that.staffId == null) {
                break Label_0095;
            }
            return false;
        }
        Label_0128: {
            if (this.type != null) {
                if (this.type.equals(that.type)) {
                    break Label_0128;
                }
            }
            else if (that.type == null) {
                break Label_0128;
            }
            return false;
        }
        Label_0161: {
            if (this.reason != null) {
                if (this.reason.equals(that.reason)) {
                    break Label_0161;
                }
            }
            else if (that.reason == null) {
                break Label_0161;
            }
            return false;
        }
        Label_0194: {
            if (this.days != null) {
                if (this.days.equals(that.days)) {
                    break Label_0194;
                }
            }
            else if (that.days == null) {
                break Label_0194;
            }
            return false;
        }
        Label_0227: {
            if (this.dateapply != null) {
                if (this.dateapply.equals(that.dateapply)) {
                    break Label_0227;
                }
            }
            else if (that.dateapply == null) {
                break Label_0227;
            }
            return false;
        }
        Label_0260: {
            if (this.datestart != null) {
                if (this.datestart.equals(that.datestart)) {
                    break Label_0260;
                }
            }
            else if (that.datestart == null) {
                break Label_0260;
            }
            return false;
        }
        Label_0293: {
            if (this.dateend != null) {
                if (this.dateend.equals(that.dateend)) {
                    break Label_0293;
                }
            }
            else if (that.dateend == null) {
                break Label_0293;
            }
            return false;
        }
        Label_0326: {
            if (this.supervisorId != null) {
                if (this.supervisorId.equals(that.supervisorId)) {
                    break Label_0326;
                }
            }
            else if (that.supervisorId == null) {
                break Label_0326;
            }
            return false;
        }
        Label_0359: {
            if (this.supervisorDate != null) {
                if (this.supervisorDate.equals(that.supervisorDate)) {
                    break Label_0359;
                }
            }
            else if (that.supervisorDate == null) {
                break Label_0359;
            }
            return false;
        }
        Label_0392: {
            if (this.headId != null) {
                if (this.headId.equals(that.headId)) {
                    break Label_0392;
                }
            }
            else if (that.headId == null) {
                break Label_0392;
            }
            return false;
        }
        Label_0425: {
            if (this.headDate != null) {
                if (this.headDate.equals(that.headDate)) {
                    break Label_0425;
                }
            }
            else if (that.headDate == null) {
                break Label_0425;
            }
            return false;
        }
        Label_0458: {
            if (this.hrId != null) {
                if (this.hrId.equals(that.hrId)) {
                    break Label_0458;
                }
            }
            else if (that.hrId == null) {
                break Label_0458;
            }
            return false;
        }
        Label_0491: {
            if (this.hrDate != null) {
                if (this.hrDate.equals(that.hrDate)) {
                    break Label_0491;
                }
            }
            else if (that.hrDate == null) {
                break Label_0491;
            }
            return false;
        }
        Label_0524: {
            if (this.checkId != null) {
                if (this.checkId.equals(that.checkId)) {
                    break Label_0524;
                }
            }
            else if (that.checkId == null) {
                break Label_0524;
            }
            return false;
        }
        Label_0557: {
            if (this.checkDate != null) {
                if (this.checkDate.equals(that.checkDate)) {
                    break Label_0557;
                }
            }
            else if (that.checkDate == null) {
                break Label_0557;
            }
            return false;
        }
        Label_0590: {
            if (this.status != null) {
                if (this.status.equals(that.status)) {
                    break Label_0590;
                }
            }
            else if (that.status == null) {
                break Label_0590;
            }
            return false;
        }
        Label_0623: {
            if (this.year != null) {
                if (this.year.equals(that.year)) {
                    break Label_0623;
                }
            }
            else if (that.year == null) {
                break Label_0623;
            }
            return false;
        }
        Label_0656: {
            if (this.period != null) {
                if (this.period.equals(that.period)) {
                    break Label_0656;
                }
            }
            else if (that.period == null) {
                break Label_0656;
            }
            return false;
        }
        if (this.stafflevel != null) {
            if (this.stafflevel.equals(that.stafflevel)) {
                return true;
            }
        }
        else if (that.stafflevel == null) {
            return true;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        int result = (this.leaveId != null) ? this.leaveId.hashCode() : 0;
        result = 31 * result + ((this.staffId != null) ? this.staffId.hashCode() : 0);
        result = 31 * result + ((this.type != null) ? this.type.hashCode() : 0);
        result = 31 * result + ((this.reason != null) ? this.reason.hashCode() : 0);
        result = 31 * result + ((this.days != null) ? this.days.hashCode() : 0);
        result = 31 * result + ((this.dateapply != null) ? this.dateapply.hashCode() : 0);
        result = 31 * result + ((this.datestart != null) ? this.datestart.hashCode() : 0);
        result = 31 * result + ((this.dateend != null) ? this.dateend.hashCode() : 0);
        result = 31 * result + ((this.supervisorId != null) ? this.supervisorId.hashCode() : 0);
        result = 31 * result + ((this.supervisorDate != null) ? this.supervisorDate.hashCode() : 0);
        result = 31 * result + ((this.headId != null) ? this.headId.hashCode() : 0);
        result = 31 * result + ((this.headDate != null) ? this.headDate.hashCode() : 0);
        result = 31 * result + ((this.hrId != null) ? this.hrId.hashCode() : 0);
        result = 31 * result + ((this.hrDate != null) ? this.hrDate.hashCode() : 0);
        result = 31 * result + ((this.checkId != null) ? this.checkId.hashCode() : 0);
        result = 31 * result + ((this.checkDate != null) ? this.checkDate.hashCode() : 0);
        result = 31 * result + ((this.status != null) ? this.status.hashCode() : 0);
        result = 31 * result + ((this.year != null) ? this.year.hashCode() : 0);
        result = 31 * result + ((this.period != null) ? this.period.hashCode() : 0);
        result = 31 * result + ((this.stafflevel != null) ? this.stafflevel.hashCode() : 0);
        return result;
    }
}
