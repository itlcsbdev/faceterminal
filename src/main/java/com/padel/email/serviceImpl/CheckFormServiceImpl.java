// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.serviceImpl;

import com.padel.email.model.CheckformEntity;
import org.springframework.beans.factory.annotation.Autowired;
import com.padel.email.repository.CheckFormRepository;
import org.springframework.stereotype.Service;
import com.padel.email.service.CheckInfoService;

@Service
public class CheckFormServiceImpl implements CheckInfoService
{
    @Autowired
    private CheckFormRepository repository;
    
    public CheckformEntity getCheckFormInfo(final String checkId) {
        return this.repository.getCheckFormInfo(checkId);
    }
    
    public CheckformEntity create(final CheckformEntity cf) {
        return this.repository.save(cf);
    }
}
