// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.serviceImpl;

import com.padel.email.model.StaffModel;
import com.padel.email.model.CoStaffEntity;
import java.util.List;
import com.padel.email.service.UserAccessService;
import org.springframework.beans.factory.annotation.Autowired;
import com.padel.email.repository.StaffRepository;
import org.springframework.stereotype.Service;
import com.padel.email.service.StaffService;

@Service
public class StaffServiceImpl implements StaffService
{
    @Autowired
    private StaffRepository repository;
    @Autowired
    private UserAccessService serviceUserAccess;
    
    public List<CoStaffEntity> findAll() {
        return this.repository.findAll();
    }
    
    public CoStaffEntity findByStaffid(final String code) {
        return this.repository.findByStaffid(code);
    }
    
    public CoStaffEntity findByEmail(final String code) {
        return this.repository.findByEmail(code);
    }
    
    public StaffModel getStaffInfoWithUserAccess(final String code) {
        final StaffModel sm = new StaffModel();
        sm.setCoStaff(this.repository.findByStaffid(code));
        sm.setUserLevel(this.serviceUserAccess.findByStaffId(code).getLevel());
        return sm;
    }
}
