// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.serviceImpl;

import com.padel.email.model.SuperviseInfoEntity;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.padel.email.repository.SuperviseRepository;
import org.springframework.stereotype.Service;
import com.padel.email.service.SuperviseService;

@Service
public class SuperviseServiceImpl implements SuperviseService
{
    @Autowired
    private SuperviseRepository repositorySupervise;
    
    public List<String> getSuperviseeIDs(final String code, final int level) {
        List<String> listA = new ArrayList<String>();
        if (level == 2) {
            listA = this.repositorySupervise.getSuperviseeIDforSupervisor(code);
        }
        else if (level == 3) {
            listA = this.repositorySupervise.getSuperviseeIDforHead(code);
        }
        return listA;
    }
    
    public SuperviseInfoEntity getSuperViseInfo(final String code) {
        return this.repositorySupervise.getSuperViseInfo(code);
    }
}
