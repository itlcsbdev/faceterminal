// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.serviceImpl;

import com.padel.email.model.LeaveInfoEntity;
import com.padel.email.service.LeaveService;
import com.padel.email.repository.LeaveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import com.padel.email.repository.LeaveInfoRepository;
import org.springframework.stereotype.Service;
import com.padel.email.service.LeaveInfoService;

@Service
public class LeaveInfoServiceImpl implements LeaveInfoService
{
    @Autowired
    private LeaveInfoRepository repository;
    @Autowired
    private LeaveRepository repositoryLeaveMaster;
    @Autowired
    private LeaveService serviceLeaveMaster;
    
    public LeaveInfoEntity findByStaffId(final String code) {
        return this.repository.findByStaffId(code);
    }
    
    public double getBalanceEligibleLeave(final String code, final String year) {
        double cnt = 0.0;
        final LeaveInfoEntity l = this.repository.findByStaffIdAndYear(code, year);
        cnt = l.getEligibleleave() + l.getBf() - this.repositoryLeaveMaster.getTotalLeaveOfTheYear(code, year);
        return cnt;
    }
    
    public LeaveInfoEntity findByStaffIdAndYear(final String code, final String year) {
        return this.repository.findByStaffIdAndYear(code, year);
    }
    
    public double getEligibleLeaveForTheMonth(final String code, final String year, final String date) {
        double cnt = 0.0;
        final LeaveInfoEntity l = this.repository.findByStaffIdAndYear(code, year);
        final double CL = l.getEligibleleave();
        final double curMonth = 3.0;
        final double beforeAddCF = CL / 12.0 * curMonth;
        final double afterAddCF = beforeAddCF + l.getBf();
        final double plusLeaveUse = cnt = afterAddCF - this.repositoryLeaveMaster.getTotalLeaveOfTheYear(code, year);
        return cnt;
    }
}
