// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.serviceImpl;

import com.padel.email.model.UserAccessEntity;
import org.springframework.beans.factory.annotation.Autowired;
import com.padel.email.repository.UserAccessRepository;
import org.springframework.stereotype.Service;
import com.padel.email.service.UserAccessService;

@Service
public class UserAccessServiceImpl implements UserAccessService
{
    @Autowired
    private UserAccessRepository repository;
    
    public UserAccessEntity findByStaffId(final String code) {
        return this.repository.findByStaffId(code);
    }
}
