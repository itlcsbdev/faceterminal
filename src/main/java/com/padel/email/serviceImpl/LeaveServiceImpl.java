// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.serviceImpl;

import java.util.Collection;
import java.util.ArrayList;
import com.padel.email.model.LeaveMasterEntity;
import java.util.List;
import com.padel.email.service.UserAccessService;
import com.padel.email.service.SuperviseService;
import org.springframework.beans.factory.annotation.Autowired;
import com.padel.email.repository.LeaveRepository;
import org.springframework.stereotype.Service;
import com.padel.email.service.LeaveService;

@Service
public class LeaveServiceImpl implements LeaveService
{
    @Autowired
    private LeaveRepository repository;
    @Autowired
    private SuperviseService superviseService;
    @Autowired
    private UserAccessService uaService;
    
    public List<LeaveMasterEntity> findAll() {
        return this.repository.findAll();
    }
    
    public LeaveMasterEntity findByLeaveId(final String code) {
        return this.repository.findByLeaveId(code);
    }
    
    public List<LeaveMasterEntity> findByStaffId(final String code) {
        return this.repository.findByStaffId(code);
    }
    
    public List<LeaveMasterEntity> findLeaveNotApproved(final String staffId) {
        return this.repository.findLeaveNotApproved(staffId);
    }
    
    public List<LeaveMasterEntity> getNewLeave(final String staffId) {
        return this.repository.getNewLeave(staffId);
    }
    
    public List<LeaveMasterEntity> getLeaveWithStatus(final String staffId, final String status) {
        return this.repository.getLeaveWithStatus(staffId, status);
    }
    
    public double getTotalLeaveOfTheYear(final String staffId, final String year) {
        return this.repository.getTotalLeaveOfTheYear(staffId, year);
    }
    
    public double getTotalSickLeaveUseOfTheYear(final String staffId, final String year) {
        return this.repository.getTotalSickLeaveUseOfTheYear(staffId, year);
    }
    
    public List<LeaveMasterEntity> getAllLeaveExcludeYou(final String staffId) {
        final List<LeaveMasterEntity> lm = new ArrayList<LeaveMasterEntity>();
        final List<String> sv = (List<String>)this.superviseService.getSuperviseeIDs(staffId, this.uaService.findByStaffId(staffId).getLevel());
        for (int j = 0; j < sv.size(); ++j) {
            System.out.print(sv.get(j));
            lm.addAll(this.repository.getAllLeaveExcludeYou(staffId, sv.get(j)));
        }
        return lm;
    }
    
    public List<LeaveMasterEntity> getUnapproveLeave() {
        return this.repository.getUnapprovedLeave();
    }
}
