// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.serviceImpl;

import org.slf4j.LoggerFactory;
import com.padel.email.model.CheckmasterEntity;
import com.padel.email.model.CoStaffEntity;
import java.util.List;
import com.padel.email.repository.CheckRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;
import com.padel.email.service.CheckService;

@Service
@Configurable
public class CheckServiceImpl implements CheckService
{
    private static final Logger log;
    @Autowired
    private CheckService checkService;
    @Autowired
    private CheckRepository repository;
    
    public List<CoStaffEntity> getAllHQStaff() {
        return this.repository.getAllHQStaff();
    }
    
    public CheckmasterEntity getCheckInfo(final String staffId, final String date) {
        return this.repository.getCheckInfo(staffId, date);
    }
    
    public String getStaffID(final int id) {
        return this.repository.getStaffID(id);
    }
    
    public void update(final double temp, final String status, final String time, final String date, final String staffId) {
        this.repository.update(temp, status, time, date, staffId);
    }
    
    public CheckmasterEntity create(final CheckmasterEntity cm) {
        return this.repository.save(cm);
    }
    
    public String getMaxID() {
        return this.repository.getMaxID();
    }
    
    public void checkout(final String time, final String date, final String staffId) {
        this.repository.checkOut(time, date, staffId);
    }
    
    static {
        log = LoggerFactory.getLogger((Class)CheckServiceImpl.class);
    }
}
