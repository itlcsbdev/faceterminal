// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.repository;

import org.springframework.data.jpa.repository.Query;
import java.util.List;
import com.padel.email.model.LeaveMasterEntity;
import org.springframework.data.repository.Repository;

public interface LeaveRepository extends Repository<LeaveMasterEntity, Integer>
{
    List<LeaveMasterEntity> findAll();
    
    LeaveMasterEntity findByLeaveId(final String code);
    
    List<LeaveMasterEntity> findByStaffId(final String code);
    
    @Query("SELECT p FROM LeaveMasterEntity p")
    List<LeaveMasterEntity> findByYearandPeriod();
    
    @Query("SELECT p FROM LeaveMasterEntity p WHERE p.staffId = ?1 AND p.status <> 'Approved'")
    List<LeaveMasterEntity> findLeaveNotApproved(final String staffId);
    
    @Query("SELECT p FROM LeaveMasterEntity p WHERE p.staffId = ?1 AND p.datestart >= CURDATE()")
    List<LeaveMasterEntity> getNewLeave(final String staffId);
    
    @Query("SELECT count(p) FROM LeaveMasterEntity p WHERE p.staffId = ?1 AND p.datestart >= CURDATE()")
    int getCountLeave(final String staffId);
    
    @Query("SELECT sum(p.days) FROM LeaveMasterEntity p WHERE p.staffId = ?1 AND p.year =  ?2 AND p.status = 'Approved' AND p.type <> 'Cuti Sakit'")
    double getTotalLeaveOfTheYear(final String staffId, final String year);
    
    @Query("SELECT sum(p.days) FROM LeaveMasterEntity p WHERE p.staffId = ?1 AND p.year =  ?2 AND p.status = 'Approved' AND p.type = 'Cuti Sakit'")
    double getTotalSickLeaveUseOfTheYear(final String staffId, final String year);
    
    @Query("SELECT p, r FROM LeaveMasterEntity p, CoStaffEntity r WHERE p.staffId = r.staffid AND p.staffId <> ?1 AND  p.staffId = ?2")
    List<LeaveMasterEntity> getAllLeaveExcludeYou(final String staffId, final String superViseeID);
    
    @Query("SELECT p FROM LeaveMasterEntity p WHERE p.staffId = ?1 AND p.status = ?2")
    List<LeaveMasterEntity> getLeaveWithStatus(final String staffId, final String status);
    
    @Query("SELECT p FROM LeaveMasterEntity p WHERE p.status NOT IN ('Approved','Rejected')")
    List<LeaveMasterEntity> getUnapprovedLeave();
}
