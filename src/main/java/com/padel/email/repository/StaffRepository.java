// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.repository;

import java.util.List;
import com.padel.email.model.CoStaffEntity;
import org.springframework.data.repository.Repository;

public interface StaffRepository extends Repository<CoStaffEntity, Integer>
{
    List<CoStaffEntity> findAll();
    
    CoStaffEntity findByStaffid(final String code);
    
    CoStaffEntity findByEmail(final String code);
}
