// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.repository;

import com.padel.email.model.UserAccessEntity;
import org.springframework.data.repository.Repository;

public interface UserAccessRepository extends Repository<UserAccessEntity, Integer>
{
    UserAccessEntity findByStaffId(final String code);
}
