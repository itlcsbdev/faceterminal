// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.Query;
import com.padel.email.model.CoStaffEntity;
import java.util.List;
import com.padel.email.model.CheckmasterEntity;
import org.springframework.data.repository.Repository;

public interface CheckRepository extends Repository<CheckmasterEntity, Integer>
{
    @Query("SELECT p FROM  CoStaffEntity p WHERE p.email <> '' AND (p.location = 'IBU PEJABAT' OR p.location = 'CAWANGAN INDERA MAHKOTA') AND p.position <> 'PENYELIA'")
    List<CoStaffEntity> getAllHQStaff();
    
    @Query("SELECT p FROM  CheckmasterEntity p WHERE p.staffId = ?1 AND p.date = ?2")
    CheckmasterEntity getCheckInfo(final String staffId, final String date);
    
    @Query("SELECT r.personCode FROM PersonEntity r WHERE r.id = ?1")
    String getStaffID(final int id);
    
    @Transactional
    @Modifying
    @Query("update CheckmasterEntity u set u.temperature = ?1, u.status = ?2, u.timevalid = ?3 where u.date = ?4 and u.staffId = ?5")
    void update(final double temp, final String status, final String time, final String date, final String staffId);
    
    @Transactional
    @Modifying
    @Query("update CheckmasterEntity u set  u.timeout = ?1 where u.date = ?2 and u.staffId = ?3")
    void checkOut(final String time, final String date, final String staffId);
    
    CheckmasterEntity save(final CheckmasterEntity cm);
    
    @Query(value = "SELECT ifnull(concat(lpad(max(id)+1,10,'0')), '0000000001') as new from checkmaster ", nativeQuery = true)
    String getMaxID();
}
