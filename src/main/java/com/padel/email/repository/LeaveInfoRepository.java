// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.repository;

import com.padel.email.model.LeaveInfoEntity;
import org.springframework.data.repository.Repository;

public interface LeaveInfoRepository extends Repository<LeaveInfoEntity, Integer>
{
    LeaveInfoEntity findByStaffId(final String code);
    
    LeaveInfoEntity findByStaffIdAndYear(final String code, final String year);
}
