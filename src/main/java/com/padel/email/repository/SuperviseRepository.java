// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.repository;

import org.springframework.data.jpa.repository.Query;
import java.util.List;
import com.padel.email.model.SuperviseInfoEntity;
import org.springframework.data.repository.Repository;

public interface SuperviseRepository extends Repository<SuperviseInfoEntity, Integer>
{
    @Query("SELECT p.staffId FROM SuperviseInfoEntity p WHERE p.supervisorId = ?1")
    List<String> getSuperviseeIDforSupervisor(final String staffId);
    
    @Query("SELECT p.staffId FROM SuperviseInfoEntity p WHERE p.headId = ?1")
    List<String> getSuperviseeIDforHead(final String staffId);
    
    @Query("SELECT p FROM SuperviseInfoEntity p WHERE p.staffId = ?1")
    SuperviseInfoEntity getSuperViseInfo(final String staffId);
}
