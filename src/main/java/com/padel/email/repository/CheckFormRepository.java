// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.repository;

import org.springframework.data.jpa.repository.Query;
import com.padel.email.model.CheckformEntity;
import org.springframework.data.repository.Repository;

public interface CheckFormRepository extends Repository<CheckformEntity, Integer>
{
    @Query("SELECT p FROM  CheckformEntity p WHERE p.checkId = ?1")
    CheckformEntity getCheckFormInfo(final String checkId);
    
    CheckformEntity save(final CheckformEntity cf);
}
