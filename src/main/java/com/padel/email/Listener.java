// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.impossibl.postgres.api.jdbc.PGConnection;
import com.impossibl.postgres.api.jdbc.PGNotificationListener;
import com.impossibl.postgres.jdbc.PGDataSource;
import com.padel.email.model.CheckformEntity;
import com.padel.email.model.CheckmasterEntity;
import com.padel.email.model.FaceResult;
import com.padel.email.service.CheckInfoService;
import com.padel.email.service.CheckService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class Listener
{
    private static final Logger log;
    static PGConnection connection;
    @Autowired
    private CheckService checkService;
    @Autowired
    private CheckInfoService checkInfoService;
    @Autowired
    private EmailSender sendService;
    
    @EventListener({ ApplicationReadyEvent.class })
    public void ListenNotify() throws InterruptedException {
    	PGNotificationListener listener = new PGNotificationListener() {

    	    @Override
    	    public void notification(int processId, String channelName, String payload) {
    	    	 ObjectMapper objectMapper = new ObjectMapper();
    	         SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
    	         SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm:ss");
    	        try {
    	             FaceResult datas = (FaceResult)objectMapper.readValue(payload, (Class)FaceResult.class);
    	             double newtemp = Double.parseDouble(datas.getData().getTemp_data());
    	             String date = datas.getData().getSwip_card_rev_time().substring(0, 10);
    	             String time = datas.getData().getSwip_card_rev_time().substring(11, 19);
    	             String staffID = String.valueOf(checkService.getStaffID(datas.getData().getPerson_id()));
    	            String status = "Gagal";
    	             CheckmasterEntity cm = checkService.getCheckInfo(staffID, date);
    	            if (cm != null) {
    	                final CheckformEntity cf = checkInfoService.getCheckFormInfo(cm.getId());
    	                if (cf.getQuestion1().equals("") && cf.getQuestion2().equals("Tidak") && cf.getQuestion3().equals("Tidak")) {
    	                    status = "Lulus";
    	                }
    	                if (newtemp > 37.4) {
    	                    status = "Gagal";
    	                }
    	                if (cm.getStatus().equals("Pending") || cm.getStatus().equals("Gagal")) {
    	                	checkService.update(newtemp, status, time, date, staffID);
    	                    System.out.println("Updated : " + staffID + "-" + newtemp + "-" + date + "-" + time + "-" + status);
    	                }
    	                else {
    	                     Date timeScan = formatTime.parse(time);
    	                     Date timeLastOut = formatTime.parse(cm.getTimeout());
    	                     Date minOut = formatTime.parse("15:00:00");
    	                     Date minSendEmail = formatTime.parse("14:30:00");
    	                    if (timeScan.after(minOut)) {
    	                         long difference = timeScan.getTime() - timeLastOut.getTime();
    	                         long diffInMin = difference / 1000L;
    	                        System.out.println("Diff : " + diffInMin);
    	                        if (cm.getTimeout().equals("00:00:00") || (!cm.getTimeout().equals("00:00:00") && diffInMin > 30L)) {
    	                        	checkService.checkout(time, date, staffID);
    	                            System.out.println("Check Out : " + staffID + "-" + date + "-" + time);
    	                            if (timeScan.after(minSendEmail)) {
    	                                try {
    	                                	sendService.sendCheckoutEmail(time, date, staffID);
    	                                }
    	                                catch (AddressException e) {
    	                                    e.printStackTrace();
    	                                }
    	                                catch (MessagingException e2) {
    	                                    e2.printStackTrace();
    	                                }
    	                                System.out.println("Email Sent : " + staffID);
    	                            }
    	                        }
    	                    }
    	                }
    	            }
    	            else if (staffID.equals("P0832") || staffID.equals("P0004")) {
    	                final String maxID = checkService.getMaxID();
    	                status = "Lulus";
    	                final CheckmasterEntity ci = new CheckmasterEntity();
    	                ci.setDate(date);
    	                ci.setId(maxID);
    	                ci.setStaffId(staffID);
    	                ci.setStatus(status);
    	                ci.setTemperature(Double.valueOf(newtemp));
    	                ci.setTimescan(time);
    	                ci.setTimevalid(time);
    	                ci.setTimeout("00:00:00");
    	                checkService.create(ci);
    	                final CheckformEntity cf2 = new CheckformEntity();
    	                cf2.setCheckId(maxID);
    	                cf2.setQuestion1("");
    	                cf2.setQuestion2("Tidak");
    	                cf2.setQuestion3("Tidak");
    	                checkInfoService.create(cf2);
    	                System.out.println("Inserted : " + staffID + "-" + newtemp + "-" + date + "-" + time + "-" + status);
    	            }
    	            else {
    	                System.out.println("Please scan QR first! - StaffID : " + staffID);
    	            }
    	        }
    	        catch (JsonProcessingException | ParseException ex2) {
    	            final Exception ex;
    	            //final Exception e3 = ex;
    	            //ex.printStackTrace();
    	        }
    	        catch (IOException e4) {
    	            e4.printStackTrace();
    	        }
    	    };
    	};
        try {
             PGDataSource dataSource = new PGDataSource();
            //dataSource.setHost("localhost");
			dataSource.setHost("192.168.254.200");
            dataSource.setPort(5432);
            dataSource.setDatabaseName("vsm");
            dataSource.setUser("lcsb");
            dataSource.setPassword("adminlcsb");
            (Listener.connection = (PGConnection)dataSource.getConnection()).addNotificationListener(listener);
            final Statement statement = Listener.connection.createStatement();
            statement.execute("LISTEN q_temp");
            statement.close();
            while (true) {
                Thread.sleep(500L);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    static {
        log = LoggerFactory.getLogger((Class)Listener.class);
    }
}
