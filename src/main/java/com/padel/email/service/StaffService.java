// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.service;

import com.padel.email.model.StaffModel;
import com.padel.email.model.CoStaffEntity;
import java.util.List;

public interface StaffService
{
    List<CoStaffEntity> findAll();
    
    CoStaffEntity findByStaffid(final String code);
    
    CoStaffEntity findByEmail(final String code);
    
    StaffModel getStaffInfoWithUserAccess(final String code);
}
