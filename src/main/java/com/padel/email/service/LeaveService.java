// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.service;

import com.padel.email.model.LeaveMasterEntity;
import java.util.List;

public interface LeaveService
{
    List<LeaveMasterEntity> findAll();
    
    List<LeaveMasterEntity> findByStaffId(final String code);
    
    LeaveMasterEntity findByLeaveId(final String code);
    
    List<LeaveMasterEntity> findLeaveNotApproved(final String staffId);
    
    List<LeaveMasterEntity> getNewLeave(final String staffId);
    
    double getTotalLeaveOfTheYear(final String staffId, final String year);
    
    double getTotalSickLeaveUseOfTheYear(final String staffId, final String year);
    
    List<LeaveMasterEntity> getAllLeaveExcludeYou(final String staffId);
    
    List<LeaveMasterEntity> getLeaveWithStatus(final String staffId, final String status);
    
    List<LeaveMasterEntity> getUnapproveLeave();
}
