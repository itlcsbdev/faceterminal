// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.service;

import com.padel.email.model.UserAccessEntity;

public interface UserAccessService
{
    UserAccessEntity findByStaffId(final String code);
}
