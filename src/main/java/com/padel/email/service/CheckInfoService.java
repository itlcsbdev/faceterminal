// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.service;

import com.padel.email.model.CheckformEntity;

public interface CheckInfoService
{
    CheckformEntity getCheckFormInfo(final String checkId);
    
    CheckformEntity create(final CheckformEntity cf);
}
