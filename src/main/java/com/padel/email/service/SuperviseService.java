// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.service;

import com.padel.email.model.SuperviseInfoEntity;
import java.util.List;

public interface SuperviseService
{
    List<String> getSuperviseeIDs(final String code, final int level);
    
    SuperviseInfoEntity getSuperViseInfo(final String code);
}
