// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.service;

import com.padel.email.model.LeaveInfoEntity;

public interface LeaveInfoService
{
    LeaveInfoEntity findByStaffId(final String code);
    
    LeaveInfoEntity findByStaffIdAndYear(final String code, final String year);
    
    double getBalanceEligibleLeave(final String code, final String year);
    
    double getEligibleLeaveForTheMonth(final String code, final String year, final String date);
}
