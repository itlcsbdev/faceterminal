// 
// Decompiled by Procyon v0.5.36
// 

package com.padel.email.service;

import com.padel.email.model.CheckmasterEntity;
import com.padel.email.model.CoStaffEntity;
import java.util.List;

public interface CheckService
{
    List<CoStaffEntity> getAllHQStaff();
    
    CheckmasterEntity getCheckInfo(final String staffId, final String date);
    
    String getStaffID(final int id);
    
    void update(final double temp, final String status, final String time, final String date, final String staffId);
    
    void checkout(final String time, final String date, final String staffId);
    
    CheckmasterEntity create(final CheckmasterEntity cm);
    
    String getMaxID();
}
